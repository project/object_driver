An utility module providing generalized Drupal object CRUD functions- 
for Drupal 5.x

To use this module you must create another small module that defines your
data.  An example module (member_list) is included for demonstration purposes.
The .install file is not needed if you already have a data table in your 
database.

The object driver module defines a single permission 'access object driver data'
User roles without this permission will not be able to see any data regardless
of whatever permissions your module defines.  This may be useful, for example,
to prevent anonymous users from seeing that you have custom data tables.

Hooks this module looks for are named with the convention:
(hook -> module name, classhook -> class name)

See the object_driver.module comments for a full list of hooks.  At the minimum,
your module must implement these hooks:

hook_data_classes(): returns an array of class names

classhook_class_define(): return a definition for the object's data table.
classhook_class_access($op): $op = 'create', 'view', 'update', 'delete'

The integer primary key of the data table must have the name classhook_id.


The array returned by classhook_class_define() can have these elements:

required:

'columns' - an array of the columns in your table.  The key is the column name,
            the value is an array with infomation about the type of data:
            
            required element for each column entry:
            
            'name'      - a human-readable name for the column

            
            optional elements:
            
            'data type' - 'varchar', 'int', etc.  Default treats data as text
            'form type' - 'select', 'radios', 'textfield', or 'textarea'
                          defaults to 'textfield'..  
            'options'   - an array of options for select or radio elements.
            'maxlength' - a max length for a textfield. Defaults to 128.
            'rows'      - a number of rows for a textarea.  Defaults to 5.
            'default'   - a default value for this column for new objects
            'required'  - TRUE or FALSE; If TRUE makes this a required form 
                          field. defaults to FALSE.

            
optional:

'table'       - the name of the SQL table- if not defined, defaults to the 
                class name.

'description' - a description of this data class.
  
'auto_menu'   - TRUE or FALSE; If TRUE, object_driver will automatically create
                 menu items with appropriate callbacks for you.  You'll almost
                 always want to define this as TRUE.

